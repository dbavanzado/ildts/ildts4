//
//  ildts4App.swift
//  ildts4
//
//  Created by LDTS Ltd. Liability Company on 23/2/23.
//  Todos los derechos reservados.
//

import SwiftUI

@main
struct ildts4App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
