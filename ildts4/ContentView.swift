//
//  ContentView.swift
//  ildts4
//
//  Created by LDTS Ltd. Liability Company on 23/2/23.
//  Todos los derechos reservados.
//

import SwiftUI

struct ContentView: View {

    @State var models: [ResponseModel] = []

    var body: some View {
        
        Section {
            Divider()
                .padding(.bottom, 10.0)
                .colorInvert()
            Label("SwiftUI iLDTS DBApp-4", systemImage: "iphone")
                .font(/*@START_MENU_TOKEN@*/.headline/*@END_MENU_TOKEN@*/)
            Label("by LDTS", systemImage: "signature")
                .font(.subheadline).padding()
        }
        
        Divider().padding(.top, 10.0).padding(.bottom, 10.0).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
        
        //AQUI ESTA EL CONTENIDO PRINCIPAL: LA LISTA VERTICAL
        VStack {

            //ANTES DE MOSTRAR ESTA VSTACK SE EJECUTARA EL onAppear QUE ESTA DEBAJO...
            List (self.models) { (model) in
                VStack{
                    let t1 = model.idGrupo ?? ""
                    let t2 = model.pec ?? ""
                    let t3 = model.clase ?? ""
                    HStack{
                        Text(t1).bold()
                        Text(t2).fontWeight(Font.Weight.light)
                    }.padding(.bottom, 2)
                    Text(t3).italic().font(Font.footnote)
                }
            }.refreshable {
                //ESTO SE EJECUTA CUANDO ARRASTRO LA LISTA HACIA ABAJO...
                loaddata()
            }
            
        }.onAppear(perform: {
            //ESTO SE EJECUTA ANTES DE QUE APAREZCA EL VStack
            loaddata()
        })
        .padding()
        
    }
    
    func loaddata() {
        //ESTA FUNCIÓN CONECTA CON LA API POR HTTPS Y RECIBE LA INFO EN JSON
        //PARA QUE PUEDA USAR LAS VARIABLE models Y SER LLAMADA DESDE body,
        //TIENE QUE DEFINIRSE DENTRO DEL MISMO STRUCT.
        guard let url: URL = URL(string: "https://lab.ldts.es/phplab/pru1.php") else {
            print("HTTPS Server Connection Error")
            return
        }
        
        var urlRequest: URLRequest =
        URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        
        //A CONTINUACIÓN CONECTAMOS...
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                print("Invalid HTTPS Response")
                return
            }
            
            //SI HA CONECTADO BIEN Y SE HA RECIBIDO LOS DATOS EN JSON EN LA VARIABLE data...
            do {
                self.models = try  //...INTENTO DECODIFICAR EL JSON QUE DEBE CUADRAR CON EL MODELO ResponseModel
                JSONDecoder().decode(
                    [ResponseModel].self, from: data)
            } catch {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
}

class ResponseModel: Codable, Identifiable {
    //ESTE ES EL MODELO CON EL QUE DECODIFICARE EL JSON QUE RECIBA POR HTTPS...
    //TIENE QUE CASAR CON LA QUERY Y EL OBJETO JSON DE LA API EN EL SERVIDOR HTTPS
    var idGrupo: String? = ""
    var pec: String? = ""
    var clase: String? = ""
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
