//
//  idlts4wtchApp.swift
//  idlts4wtch Watch App
//
//  Created by Josemaria Carazo Abolafia on 24/2/23.
//

import SwiftUI

@main
struct idlts4wtch_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
